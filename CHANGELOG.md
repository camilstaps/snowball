# Changelog

#### v0.2.2

- Chore: allow base 3.x.

#### v0.2.1

- Chore: allow system 2.

## v0.2.0

- Chore: update to base 2.0.

## v0.1.0

First tagged version.
