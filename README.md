# Snowball

A [Clean][] library to interface with the [Snowball][] stemming library.

Snowball is a library to stem words in different languages. Stemming is the
process to strip words of functional affixes so that a stem remains, which is
closer to the concept of a lexeme. This is useful in information retrieval.

## Author & License
This library is written, maintained, and copyright &copy; by [Camil Staps][].

The Clean interface is licensed under AGPL v3; see the [LICENSE](LICENSE) file.

This repository also includes the Snowball sources (and the distributed version
includes object code built from these sources), which is licensed under a
3-clause BSD license (see
[libstemmer_c-2.2.0/COPYING](libstemmer_c-2.2.0/COPYING), also for copyright
details).

[Camil Staps]: https://camilstaps.nl
[Clean]: https://clean-lang.org
[Snowball]: https://snowballstem.org
