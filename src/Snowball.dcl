definition module Snowball

/**
 * This file is part of the Snowball Clean library.
 *
 * The Snowball Clean library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * The Snowball Clean library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the Snowball Clean library. If not, see
 * <https://www.gnu.org/licenses/>.
 */

/**
 * Create a new Snowball stemmer. Currently this library uses Snowball 2.2.0.
 * The stemmer is created for the UTF-8 encoding.
 *
 * The stemmer is a simple function of type `String -> ?#String`. This function
 * may fail if Snowball runs out of memory.
 *
 * Because stemmer creation is an expensive operation, you should attempt to
 * use `createStemmer` only once and store the reference to the stemmer
 * throughout your program, rather than using `createStemmer` each time a word
 * needs to be stemmed.
 *
 * @param The language or algorithm to use for the stemmer, e.g. `english`.
 *   See the Snowball website for a list: https://snowballstem.org/algorithms/.
 *   Alternatively an ISO 639 code may be used. This parameter must be supplied
 *   in lower case.
 * @result Stemmer creation may fail (return `?None`) if the requested language
 *   or algorithm is not recognized.
 */
createStemmer :: !String -> ?(String -> ?#String)
