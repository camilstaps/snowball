implementation module Snowball

/**
 * This file is part of the Snowball Clean library.
 *
 * The Snowball Clean library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, version 3 of the License.
 *
 * The Snowball Clean library is distributed in the hope that it will be
 * useful, but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU Affero
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with the Snowball Clean library. If not, see
 * <https://www.gnu.org/licenses/>.
 */

import StdEnv
import _SystemStrictMaybes

import System._Finalized
import System._Pointer

import code from "libstemmer.a"

:: Snowball :== Finalized Pointer

createStemmer :: !String -> ?(String -> ?#String)
createStemmer algo
	# ptr = sb_stemmer_new (packString algo) 0
	| ptr == 0
		= ?None
		= ?Just (stem (finalize ptr sb_stemmer_delete ptr))
where
	sb_stemmer_new :: !String !Pointer -> Pointer
	sb_stemmer_new _ _ = code {
		ccall sb_stemmer_new "sp:p"
	}

	sb_stemmer_delete :: Pointer
	sb_stemmer_delete = code {
		pushL sb_stemmer_delete
	}

stem :: !Snowball !String -> ?#String
stem stemmer word = fst (withFinalizedValue stem` stemmer)
where
	stem` stemmer
		# word_ptr = sb_stemmer_stem stemmer word (size word)
		| word_ptr == 0 = ?|None
		# len = sb_stemmer_length stemmer
		= ?|Just (derefCharArray word_ptr len)

	sb_stemmer_stem :: !Pointer !String !Int -> Pointer
	sb_stemmer_stem _ _ _ = code {
		ccall sb_stemmer_stem "psI:p"
	}

	sb_stemmer_length :: !Pointer -> Int
	sb_stemmer_length _ = code {
		ccall sb_stemmer_length "p:I"
	}
