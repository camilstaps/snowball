module test

import StdEnv
import _SystemStrictMaybes

import System.CommandLine

import Snowball

Start w = case createStemmer "english" of
	?None -> abort "Failed to create stemmer\n"
	?Just stem -> setReturnCode
		(if ((stem "testing") =: (?#Just "test")) 0 1)
		w
